from dataclasses import dataclass


@dataclass
class Iris:
    CODE_IRIS: str
    DEPCOM: str = ""
    TYP_IRIS: str = ""
    MODIF_IRIS: str = "0"
    LIB_IRIS: str = ""


@dataclass
class PassageCom:
    CODGEO_start: str
    CODGEO_end: str
