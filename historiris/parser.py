import logging
import sys

from typing import List

import numpy as np
import pandas as pd

from historiris import utils
from historiris.settings import settings


log = logging.getLogger(__name__)


def compare_common_lines(df_start: pd.DataFrame, df_end: pd.DataFrame, slug: str) -> None:
    """
    Compare les lignes communes au niveau du code IRIS entre les deux dataframes.

    Les colonnes avec des valeurs différentes sont isolées et traitées en fonction
    des règles de gestion exprimées dans `IGNORED_DIFF_COLS` et `IGNORED_DIFF_COLS_YEARS`
    (cf settings.toml).
    """
    merge = df_start.merge(df_end, how="outer")
    diff = merge[merge.duplicated(subset=["CODE_IRIS"], keep=False)]
    diff = diff.sort_values("CODE_IRIS")

    def _compare(_df):
        diff_cols = []
        # les lignes sont parfois en triple, cela ne nous intéresse pas si elles sont identiques
        _df.drop_duplicates(inplace=True)
        # si on a moins de deux lignes différentes pour un code IRIS, inutile de continuer la comparaison
        if _df.shape[0] < 2:
            return diff_cols
        # on vérifie qu'on a seulement deux lignes pour un même code IRIS
        assert _df.shape[0] == 2, f">2 instances pour un code IRIS\n{_df}"
        comp = _df.iloc[0].compare(_df.iloc[1], result_names=("start", "end"))
        if not comp.empty:
            diff_cols = [c for c in comp.index.to_list() if c not in diff_cols]
        return diff_cols

    diff_cols = diff.groupby("CODE_IRIS").apply(_compare).values
    # mise à plat (flatten) et dédoublonnage du tableau des listes de colonnes différentes
    diff_cols = set([c for clist in diff_cols for c in clist])
    log.debug(f"diff_cols\n{diff_cols}")
    for diff_col in diff_cols:
        if diff_col in settings.IGNORED_DIFF_COLS:
            log.info(f"Différence sur la colonne {diff_col} détectée, ignorée globalement")
        elif diff_col in settings.IGNORED_DIFF_COLS_YEARS.get(slug, []):
            log.warning(f"Différence sur la colonne {diff_col} détectée, ignorée localement")
        else:
            log.critical(f"Différence non gérée sur la colonne {diff_col}")

            def _col_compare(df, diff_col):
                df = df[["CODE_IRIS", diff_col]]
                return df.iloc[0].compare(df.iloc[1], result_names=("_end", "_start"))

            col_diff = diff.groupby("CODE_IRIS").apply(_col_compare, diff_col)
            log.debug(f"col_diff\n{col_diff}")
            sys.exit(1)


def compute_skl_diff(df_start: pd.DataFrame, df_end: pd.DataFrame) -> pd.DataFrame:
    """
    Calcule un dataframe de comparaison des millésimes sur le "squelette".

    Le squelette est le couple ("CODE_IRIS", "DEPCOM") pour chaque millésime, DEP_COM étant
    le code INSEE de la commune.

    On retourne les différences entre les deux millésimes sur ce squelette dans un dataframe.
    """
    skl_start = df_start[["CODE_IRIS", "DEPCOM"]]
    skl_end = df_end[["CODE_IRIS", "DEPCOM"]]
    skl_merge = skl_end.merge(skl_start, how="outer", on=["CODE_IRIS"], suffixes=("_end", "_start"))
    skl_diff = skl_merge[skl_merge["DEPCOM_end"] != skl_merge["DEPCOM_start"]]

    log.info(f"{skl_diff.shape[0]} différences détectées sur le couple (CODE_IRIS, DEPCOM)")
    log.debug(f"skl_diff\n{skl_diff}")

    return skl_diff


def get_modifications(end_year: str) -> pd.DataFrame:
    """
    Retourne les modifications documentées pour une année donnée.
    """
    modifs = utils.load_iris_modifications_from_compilation(end_year)
    assert modifs.shape[0] > 0, f"Aucune modification documentée pour {end_year}"
    return modifs


def parse_documentation(start: str, end: str, df_end: pd.DataFrame, skl_diff: pd.DataFrame):
    """
    Crée un table de passage en réconciliant les modifications documentées pour l'année `end`
    et notre comparaison sur le squelette `skl_diff`.
    """
    modifs = get_modifications(end)

    modifs_in_skl = modifs[
        modifs["IRIS_INI"].isin(skl_diff["CODE_IRIS"]) |
        modifs["IRIS_FIN"].isin(skl_diff["CODE_IRIS"])
    ]
    log.debug(f"modifs_in_skl\n{modifs_in_skl}")

    # modifications non trouvées dans skl et implicant un changement d'IRIS
    # on ne traite pas ces modifications mais on affiche un avertissement
    modifs_not_in_skl = modifs[modifs["IRIS_INI"] != modifs["IRIS_FIN"]]
    modifs_not_in_skl = modifs_not_in_skl[
        ~modifs_not_in_skl["IRIS_INI"].isin(skl_diff["CODE_IRIS"]) &
        ~modifs_not_in_skl["IRIS_FIN"].isin(skl_diff["CODE_IRIS"])
    ]
    if modifs_not_in_skl.shape[0] > 0:
        log.warning("%s modifications documentées impliquant un changement d'IRIS n'ont pas été rattachées "
                    "aux différences détectées sur le squelette.", modifs_not_in_skl.shape[0])
        log.debug(f"modifs_not_in_skl\n{modifs_not_in_skl}")

    df_passage_doc = modifs_in_skl[
        ["IRIS_INI", "IRIS_FIN", "NATURE_MODIF", "COM_INI", "COM_FIN", "ANNEE_MODIF", "MODIF_IRIS"]
    ].rename(columns={
        "IRIS_INI": f"CODE_IRIS_{start}",
        "IRIS_FIN": f"CODE_IRIS_{end}",
        "COM_INI": f"DEPCOM_{start}",
        "COM_FIN": f"DEPCOM_{end}",
    })
    df_passage_doc["SOURCE"] = "INSEE_DOC"

    # on récupère le `TYP_IRIS` depuis la source `end`
    # *ATTENTION* : ce merge supprime les lignes qui n'ont pas de correspondance sur le CODE_IRIS_end dans `df_end`
    cols = ["CODE_IRIS", "TYP_IRIS"]
    df_passage_doc = df_passage_doc.merge(
        df_end[cols], left_on=f"CODE_IRIS_{end}", right_on="CODE_IRIS", validate="m:1",
    )
    df_passage_doc = df_passage_doc.drop(columns=["CODE_IRIS"])

    log.info("Table de passage sur la documentation créée.")
    log.debug(f"df_passage_doc\n{df_passage_doc}")

    return df_passage_doc


def handle_irisees(df_end, end, df, non_z):
    """
    Gère les cas de communes irisées sur `df_end` dans `parse_cog`
    NB : fonction non utilisée à date, cf parse_cog
    """
    # on ne garde que les lignes où MODIF_IRIS <> 0, ce qui devrait correspondre à l'IRIS
    # qui a été modifié sur le millésime `end`
    non_z_extended = df[df["TYP_IRIS"] != "Z"].merge(
        df_end[["CODE_IRIS", "MODIF_IRIS"]], left_on=f"CODE_IRIS_{end}", right_on=["CODE_IRIS"],
        suffixes=("_hh", "_insee")
    )
    non_z_keep = non_z_extended[
        ~non_z_extended["MODIF_IRIS_insee"].isin(["0", "00"])
    ].copy()
    non_z_keep = non_z_keep.assign(**{
        "NATURE_MODIF": "Fusion de communes irisées",
        "MODIF_IRIS": "HH_FUSION_IRISEE",
    })
    df.drop(non_z.index, inplace=True)
    log.warning(f"handle_irisees a gardé {non_z_keep.shape[0]} lignes sur {non_z_extended.shape[0]}")
    log.debug(f"non_z_keep\n{non_z_keep}")
    # on enlève les résidus du merge pour recoller au format de la table de passage
    non_z_keep.drop(columns=["MODIF_IRIS_hh", "MODIF_IRIS_insee", "CODE_IRIS"], inplace=True)
    return pd.concat([df, non_z_keep])


def parse_cog(
    start: str, end: str, df_start: pd.DataFrame, df_end: pd.DataFrame, unexplained: pd.DataFrame,
) -> pd.DataFrame:
    """
    Crée un table de passage en tentant d'expliquer les modifications de `unexplained` par un traitement
    du fichier de passage des communes (COG).
    """
    log.debug("Chargement du fichier des communes...")
    df_com = utils.load_communes_migration_table(is_1999=(start == "1999"))

    # on s'intéresse aux cas où `DEPCOM_end` est manquant dans `unexplained`
    # on cherche alors à récupérer le `DEPCOM_end` via le `DEPCOM_start` dans le fichier des communes
    # puis on récupère dans `df_end` l'éventuel IRIS associé à `DEPCOM_end`
    unexplained_missing_end = unexplained.merge(
        df_com[[f"CODGEO_{start}", f"CODGEO_{end}"]],
        left_on="DEPCOM_start", right_on=f"CODGEO_{start}"
    ).drop_duplicates()[["CODE_IRIS", f"CODGEO_{end}", f"CODGEO_{start}"]].dropna()

    cols = ["CODE_IRIS", "DEPCOM", "TYP_IRIS"]
    df_passage_missing_end = df_end[cols].merge(
        unexplained_missing_end, left_on="DEPCOM", right_on=f"CODGEO_{end}", suffixes=("_end", "_start")
    )
    # la clé de jointure a la même valeur que son match `CODEGEO_{end}`
    df_passage_missing_end.drop(columns=["DEPCOM"], inplace=True)
    # on ajoute les attributs arbitraires qui nous intéressent pour documenter les passages
    df_passage_missing_end = df_passage_missing_end.assign(**{
        "ANNEE_MODIF": str(end),
        # on vérifie que all == Z plus tard, sinon on applique un traitement spécial et on change le message
        "NATURE_MODIF": "Fusion de communes non irisées",
        "MODIF_IRIS": "HH_FUSION",
        "SOURCE": "HH_INSEE_COG",
    })
    # on renomme les colonnes pour coller au format de la table de passage
    df_passage_missing_end = df_passage_missing_end.rename(columns={
        f"CODGEO_{end}": f"DEPCOM_{end}",
        f"CODGEO_{start}": f"DEPCOM_{start}",
        "CODE_IRIS_end": f"CODE_IRIS_{end}",
        "CODE_IRIS_start": f"CODE_IRIS_{start}",
    })

    # on vérifie qu'on ne traite automatiquement que des communes non irisées
    non_z_end = df_passage_missing_end[df_passage_missing_end["TYP_IRIS"] != "Z"]
    assert non_z_end.shape[0] == 0, "TYPE_IRIS <> Z dans df_passage_missing_start"
    # sinon on applique un traitement spécial
    # NB : traitement non nécessaire à date, commenté
    # if non_z_end.shape[0] > 0:
    #     log.warning("Communes irisées détectées dans `df_passage_missing_end`")
    #     log.debug(f"non_z_end\n{non_z_end}")
    #     df_passage_missing_end = handle_irisees(df_end, end, df_passage_missing_end, non_z_end)

    log.debug(f"df_passage_missing_end\n{df_passage_missing_end}")

    # on s'intéresse ensuite aux cas où `DEPCOM_end` est manquant dans `unexplained` ET dont l'IRIS
    # n'a pas été traité dans df_passage_missing_end
    # on cherche alors à récupérer le `DEPCOM_end` via le `DEPCOM_start` dans le fichier des communes
    # puis on récupère dans `df_end` l'éventuel IRIS associé à `DEPCOM_end`
    # NB: ce cas n'est utile que pour 1999-2008
    unexplained_missing_start = unexplained.merge(
        df_com[[f"CODGEO_{start}", f"CODGEO_{end}"]],
        left_on="DEPCOM_end", right_on=f"CODGEO_{end}"
    ).drop_duplicates()[["CODE_IRIS", f"CODGEO_{end}", f"CODGEO_{start}"]].dropna()
    unexplained_missing_start = unexplained_missing_start[
        (unexplained_missing_start[f"CODGEO_{end}"] != unexplained_missing_start[f"CODGEO_{start}"]) &
        ~unexplained_missing_start["CODE_IRIS"].isin(df_passage_missing_end[f"CODE_IRIS_{end}"])
    ]
    log.debug(f"unexplained_missing_start\n{unexplained_missing_start}")

    # on ne connait pas le TYP_IRIS pour 1999, on le passe arbitrairement à Z
    if start == "1999":
        df_start["TYP_IRIS"] = "Z"

    cols = ["CODE_IRIS", "DEPCOM", "TYP_IRIS"]
    df_passage_missing_start = df_start[cols].merge(
        unexplained_missing_start, left_on="DEPCOM", right_on=f"CODGEO_{start}", suffixes=("_start", "_end")
    )
    # la clé de jointure a la même valeur que son match `CODEGEO_{end}`
    df_passage_missing_start.drop(columns=["DEPCOM"], inplace=True)
    # on ajoute les attributs arbitraires qui nous intéressent pour documenter les passages
    df_passage_missing_start = df_passage_missing_start.assign(**{
        "ANNEE_MODIF": str(end),
        # on vérifie que all == Z plus tard
        "NATURE_MODIF": "Fusion de communes non irisées",
        "MODIF_IRIS": "HH_FUSION",
        "SOURCE": "HH_INSEE_COG",
    })
    # on renomme les colonnes pour coller au format de la table de passage
    df_passage_missing_start = df_passage_missing_start.rename(columns={
        f"CODGEO_{end}": f"DEPCOM_{end}",
        f"CODGEO_{start}": f"DEPCOM_{start}",
        "CODE_IRIS_end": f"CODE_IRIS_{end}",
        "CODE_IRIS_start": f"CODE_IRIS_{start}",
    })
    log.debug(f"df_passage_missing_start\n{df_passage_missing_start}")

    # on vérifie qu'on ne traite que des types Z sur start
    assert (df_passage_missing_start["TYP_IRIS"] == "Z").all(), "TYPE_IRIS <> Z dans df_passage_missing_start"

    # concaténation des deux tables de passage
    df_passage_com = pd.concat([df_passage_missing_end, df_passage_missing_start])
    log.debug(f"df_passage_com\n{df_passage_com}")

    return df_passage_com


def handle_unexplained(start: str, end: str, df_end: pd.DataFrame, unexplained_left: pd.DataFrame) -> pd.DataFrame:
    """
    Gère les derniers cas non expliqués par les précédentes approches.
    """
    if unexplained_left.shape[0] == 0:
        return pd.DataFrame()

    unexplained_left = filter_depcoms(end, unexplained_left)

    # `DEPCOM_end` existe mais pas `DEPCOM_start` et nos réconciliations précédentes via le COG n'ont rien retourné
    # on peut donc supposer qu'on est dans le cas d'une commune et d'un IRIS nouvellement créé
    if not unexplained_left["DEPCOM_end"].isna().all() and unexplained_left["DEPCOM_start"].isna().all():
        df_passage_unexplained = unexplained_left
        df_passage_unexplained = df_passage_unexplained.rename(columns={
            "DEPCOM_end": f"DEPCOM_{end}",
            "DEPCOM_start": f"DEPCOM_{start}",
            "CODE_IRIS": f"CODE_IRIS_{end}"
        })
        df_passage_unexplained = df_passage_unexplained.assign(**{
            f"CODE_IRIS_{start}": np.nan,
            "ANNEE_MODIF": str(end),
            "NATURE_MODIF": "Nouvelle commune",
            "MODIF_IRIS": "HH_CREATION",
            "SOURCE": "HH_UNEXPLAINED",
        })
        # on récupère le `TYP_IRIS` depuis la source `end`
        cols = ["CODE_IRIS", "TYP_IRIS"]
        df_passage_unexplained = df_passage_unexplained.merge(
            df_end[cols], left_on=f"CODE_IRIS_{end}", right_on="CODE_IRIS", validate="m:1",
        )
        df_passage_unexplained = df_passage_unexplained.drop(columns=["CODE_IRIS"])
        log.debug(f"df_passage_unexplained\n{df_passage_unexplained}")
        return df_passage_unexplained

    return pd.DataFrame()


def filter_depcoms(end: str, df: pd.DataFrame) -> pd.DataFrame:
    """
    Nettoie le dataframe sur les DEPCOM spécifiés dans la configuration pour `end`
    """
    if end in settings.FILTER_DEPCOMS.end_years:
        filter = settings.FILTER_DEPCOMS.depcoms
        log.warning(f"Ignore les différences sur les DEPCOM {filter} comme spécifié dans la configuration.")
        df = df[
            ~df["DEPCOM_start"].isin(filter) &
            ~df["DEPCOM_end"].isin(filter)
        ]
    return df


def build_final_table(
    start: str, end: str, skl_diff: pd.DataFrame, df_passage_list: List[pd.DataFrame],
) -> pd.DataFrame:
    """
    Construis la table de passage finale à partir de la liste des différent cas de passage.
    Vérifie également que tous les cas sont traités.
    """
    df_passage = pd.concat(df_passage_list)
    log.debug(f"df_passage\n{df_passage}")

    unreconcilied = skl_diff[
        ~skl_diff["CODE_IRIS"].isin(df_passage[f"CODE_IRIS_{start}"]) &
        ~skl_diff["CODE_IRIS"].isin(df_passage[f"CODE_IRIS_{end}"])
    ]

    unreconcilied = filter_depcoms(end, unreconcilied)

    if unreconcilied.shape[0] > 0:
        log.critical("skl_diff pas complètement reconcilié")
        log.debug(f"unreconcilied\n{unreconcilied}")
        sys.exit(1)

    log.info("Table créée avec succès.")

    return df_passage.sort_values(by=[f"CODE_IRIS_{start}", f"CODE_IRIS_{end}"])


def sanity_check(start: str, end: str, slug: str, df_passage: pd.DataFrame):
    """
    Applique des contrôles génériques de cohérence à la table de passage
    """
    def _validate(row):
        # le code IRIS doit commencer par le DEPCOM
        assert row[f"CODE_IRIS_{start}"].startswith(row[f"DEPCOM_{start}"]), \
            f"CODE_IRIS_start non cohérent avec DEPCOM_start\n{row}"
        assert row[f"CODE_IRIS_{end}"].startswith(row[f"DEPCOM_{end}"]), \
            f"CODE_IRIS_end non cohérent avec DEPCOM_end\n{row}"
        # le DEPCOM ne devrait pas changer de département
        if row[f"DEPCOM_{start}"][:2] != row[f"DEPCOM_{end}"][:2]:
            change_slug = f"{row[f'DEPCOM_{start}']}_{row[f'DEPCOM_{end}']}"
            if change_slug in settings.DEPARTMENT_CHANGES.get(slug, []):
                log.info(f"Changement de département {change_slug} détecté mais documenté.")
            else:
                log.critical(f"DEPCOM_start non cohérent avec DEPCOM_end (changement de département)\n{row}")
                sys.exit(1)

    df_passage[
        ~df_passage[f"CODE_IRIS_{start}"].isna() &
        ~df_passage[f"DEPCOM_{start}"].isna()
    ].apply(_validate, axis=1)
    log.info("Table de passage validée.")
