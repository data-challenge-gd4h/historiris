import logging

from pathlib import Path

import coloredlogs
import numpy as np
import pandas as pd

from minicli import cli, run

from historiris import utils, parser, parser_legacy
from historiris.settings import settings


log = logging.getLogger(__name__)


@cli
def parse_years(start: str, end: str, verbose: bool = False, export_path: Path = Path("data/iris/output")):
    """
    Crée une table de passage entre les années `start` et `end`.
    """
    coloredlogs.install(level="DEBUG" if verbose else "INFO")

    log.info(f"Traitement des millésimes {start} — {end}")
    slug = f"{start}_{end}"

    log.debug("Chargement des sources...")
    df_start = utils.load_iris_list(
        start, path=settings.IRIS_REF_FILES[start],
        args={"skiprows": settings.IRIS_REF_FILES_SKIPROWS.get(start, 5)},
    )
    df_end = utils.load_iris_list(
        end, path=settings.IRIS_REF_FILES[end],
        args={"skiprows": settings.IRIS_REF_FILES_SKIPROWS.get(end, 5)},
    )

    parser.compare_common_lines(df_start, df_end, slug)

    skl_diff = parser.compute_skl_diff(df_start, df_end)

    # traitement spécifique pour les millésimes avant 2012, aka "legacy"
    is_legacy = int(end) <= 2012
    if is_legacy:
        modifs_in_skl = parser_legacy.get_modifs_in_skl(df_end, skl_diff)

        df_passage_lib_iris = parser_legacy.merge_on_lib_iris(start, end, df_start, modifs_in_skl)

        # modifications non réconciliées sur LIB_IRIS
        unmatched = modifs_in_skl[~modifs_in_skl["CODE_IRIS"].isin(df_passage_lib_iris[f"CODE_IRIS_{end}"])]

        df_passage_doc = parser_legacy.parse_documentation(start, end, df_end, unmatched)

        # préparation des cas inexpliqués restants pour traitement via le COG
        unexplained = skl_diff[
            ~skl_diff.CODE_IRIS.isin(df_passage_doc[f"CODE_IRIS_{start}"]) &
            ~skl_diff.CODE_IRIS.isin(df_passage_doc[f"CODE_IRIS_{end}"]) &
            ~skl_diff.CODE_IRIS.isin(df_passage_lib_iris[f"CODE_IRIS_{start}"]) &
            ~skl_diff.CODE_IRIS.isin(df_passage_lib_iris[f"CODE_IRIS_{end}"])
        ]
        log.info(f"{unexplained.shape[0]} modifications non expliquées dans la documentation ou via le merge LIB_IRIS")
        log.debug(f"unexplained\n{unexplained}")
    else:
        df_passage_lib_iris = pd.DataFrame()

        df_passage_doc = parser.parse_documentation(start, end, df_end, skl_diff)

        unexplained = skl_diff[
            ~skl_diff["CODE_IRIS"].isin(df_passage_doc[f"CODE_IRIS_{start}"]) &
            ~skl_diff["CODE_IRIS"].isin(df_passage_doc[f"CODE_IRIS_{end}"])
        ]
        log.info(f"{unexplained.shape[0]} modifications non expliquées dans la documentation")
        log.debug(f"unexplained\n{unexplained}")

    df_passage_com = parser.parse_cog(start, end, df_start, df_end, unexplained)

    unexplained_left = unexplained[
        ~unexplained["CODE_IRIS"].isin(df_passage_com[f"CODE_IRIS_{start}"]) &
        ~unexplained["CODE_IRIS"].isin(df_passage_com[f"CODE_IRIS_{end}"])
    ]
    log.info(f"{unexplained_left.shape[0]} modifications encore non expliquées")
    log.debug(f"unexplained_left\n{unexplained_left}")

    df_passage_unexplained = parser.handle_unexplained(start, end, df_end, unexplained_left)

    df_passage = parser.build_final_table(
        start, end, skl_diff, [
            df_passage_lib_iris, df_passage_doc, df_passage_com, df_passage_unexplained,
        ]
    )

    parser.sanity_check(start, end, slug, df_passage)

    export_path = export_path / f"passage_{slug}.csv"
    log.info(f"Export de la table vers {export_path}.")
    df_passage.to_csv(export_path, index=False)


def accumulate_table(start: int, end: int, df: pd.DataFrame, df_acc: pd.DataFrame) -> pd.DataFrame:
    """
    Fusionne la table `df` avec la table `df_acc` sur `CODE_IRIS_{end}`.
    """
    cols = [f"CODE_IRIS_{end}", f"CODE_IRIS_{start}"]
    if df_acc.empty:
        return df[cols]
    # TODO: add DEPCOM
    df_acc = df_acc.merge(df[cols], on=f"CODE_IRIS_{end}", how="outer")
    return df_acc


def load_or_build_table(start: int, end: int, verbose: bool = False, force_refresh: bool = False):
    slug = f"{start}_{end}"
    path = Path(f"data/iris/output/passage_{slug}.csv")
    if not path.exists() or force_refresh:
        log.warning(f"La table {slug} n'existe pas ou rafraichissement demandé, lancement de la création")
        parse_years(str(start), str(end), verbose=verbose)
    log.info(f"Chargement de la table {slug}")
    return pd.read_csv(path, dtype=str)


@cli
def build_table(
    start: int = 1999, end: int = 2022, verbose: bool = False, export_path: Path = "data/iris/output",
    force_refresh: bool = False,
):
    """
    Construit une table de passage entre `start` et `end` en utilisant les tables de passage intermédiaires.
    Ex : 2012 - 2014 utilisera 2012 - 2013 et 2013 - 2014.
    """
    coloredlogs.install(level="DEBUG" if verbose else "INFO")

    global_slug = f"{start}_{end}"
    log.info(f"Construction de la table {global_slug}")

    df_acc = pd.DataFrame()
    sort_cols = []

    for year in reversed(range(start, end)):
        _start = year
        _end = year + 1
        if _end == 2008:
            _start = 1999
        df = load_or_build_table(_start, _end, verbose=verbose, force_refresh=force_refresh)
        # spécifié `"NA"` plutôt que `np.nan` afin de différencier les cas où une commune est créée des cas où
        # une valeur n'est pas spécifiquement présente pour un millésime
        df = df.fillna(value="NA")
        df_acc = accumulate_table(_start, _end, df, df_acc)
        sort_cols.append(f"CODE_IRIS_{_end}")
        if _start == 1999:
            break

    sort_cols.append(f"CODE_IRIS_{_start}")

    # on reporte en amont et en aval la première ou la dernière valeur présente
    df_acc = df_acc.fillna(method="backfill", axis=1)
    df_acc = df_acc.fillna(method="pad", axis=1)
    # on réassigne `"NA"` à `np.nan` maintenant que `fillna` a été utilisé pour remplir les trous
    df_acc = df_acc.replace("NA", np.nan)

    # on trie la table par code IRIS successifs
    df_acc = df_acc.sort_values(by=sort_cols)

    log.debug(f"df_acc\n{df_acc}")

    export_path = export_path / f"table_passage_{global_slug}.csv"
    df_acc.to_csv(export_path, index=False)


@cli
def compare_cli_nb(start=2012, end=2022, verbose=False) -> bool:
    """
    Compare la sortie d'un ou plusieurs notebooks avec celle du script `parse_years`.
    """
    coloredlogs.install(level="DEBUG" if verbose else "INFO")

    success = True

    for year in reversed(range(start, end)):
        slug = f"{year}_{year + 1}"

        df_cli = load_or_build_table(year, year + 1)
        df_nb = pd.read_csv(f"notebooks/output/passage_{slug}_nb.csv", dtype=str)

        cols = [f"CODE_IRIS_{year}", f"CODE_IRIS_{year + 1}", f"DEPCOM_{year}", f"DEPCOM_{year + 1}"]
        diff_cli_nb = df_cli[cols].merge(df_nb[cols], how="outer", indicator=True)
        diff_cli_nb = diff_cli_nb[diff_cli_nb["_merge"] != "both"]
        diff_cli_nb = diff_cli_nb.replace("left_only", "cli_only").replace("right_only", "nb_only")

        if not diff_cli_nb.shape[0] == 0:
            log.critical(f"❌ Différences détectées sur {slug}")
            log.debug(f"diff_cli_nb\n{diff_cli_nb}")
            success = False
        else:
            log.info(f"✅ Tables {slug} similaires")

    return success


if __name__ == "__main__":
    run()
