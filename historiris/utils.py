from pathlib import Path

import pandas as pd


def get_data_path() -> Path:
    """Retourne le _Path_ vers le dossier contenant les données de référence"""
    return Path(__file__).parent / "../data"


def load_iris_list(year: int, args: dict = {}, extension: str = "xlsx", path: str = None) -> pd.DataFrame:
    """Charge la liste des IRIS depuis un fichier Excel de référence dans un Dataframe"""
    base_path = get_data_path()
    # saute 5 lignes, peut être adapté en fonction de l'année si besoin
    args = {**args, **{"skiprows": 5}} if not args.get("skiprows") else args
    # parse en tant que `str` sauf si spécifié autrement
    args = {**args, **{"dtype": str}} if not args.get("dtype") else args
    path = path or f"iris/reference_IRIS_geo{year}.{extension}"
    return pd.read_excel(base_path / path, **args).drop_duplicates()


def load_iris_modifications(
    year: int, args: dict = {}, sheet_name: str = "Modifications_IRIS", extension: str = "xlsx",
    path: str = None,
) -> pd.DataFrame:
    """Charge la liste des modifications d'IRIS depuis le fichier de référence d'un millésime dans un DataFrame"""
    base_path = get_data_path()
    args["sheet_name"] = sheet_name
    # saute 5 lignes, peut être adapté en fonction de l'année si besoin
    args = {**args, **{"skiprows": 5}} if not args.get("skiprows") else args
    # parse en tant que `str` sauf si spécifié autrement
    args = {**args, **{"dtype": str}} if not args.get("dtype") else args
    path = path or f"iris/reference_IRIS_geo{year}.{extension}"
    return pd.read_excel(base_path / path, **args)


def load_iris_modifications_from_compilation(year: str, is_legacy: bool = False) -> pd.DataFrame:
    """Charge la liste des modifications d'IRIS depuis notre fichier consolidé"""
    base_path = get_data_path()
    if is_legacy:
        file_path = "modifications_1999_2012.csv"
    else:
        file_path = "modifications_2012_2022.csv"
    modifs = pd.read_csv(base_path / "iris/output" / file_path, dtype=str)
    modifs = modifs.rename(columns={"annee_modif": "ANNEE_MODIF"})
    return modifs[modifs["ANNEE_MODIF"] == year]


def load_communes_migration_table(is_1999: bool = False) -> pd.DataFrame:
    """Charge la table de passage annuelle des communes dans un DataFrame"""
    base_path = get_data_path()

    if is_1999:
        return pd.read_csv(base_path / "communes/correspondance_1999_2008.csv", dtype=str)

    return pd.read_excel(base_path / "communes/table_passage_annuelle_2023.xlsx", skiprows=5, dtype=str)
