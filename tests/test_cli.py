import tempfile

import pytest

from pathlib import Path

from historiris.cli import run, compare_cli_nb


@pytest.mark.parametrize("start,end,expected_size", [
    ("2021", "2022", 16),
])
def test_parse_years(start, end, expected_size):
    with tempfile.TemporaryDirectory() as tdir:
        run("parse_years", start, end, export_path=Path(tdir))
        with (Path(tdir) / f"passage_{start}_{end}.csv").open() as f:
            assert sum(1 for _ in f) == expected_size


@pytest.mark.parametrize("start,end,expected_size", [
    ("2009", "2010", 15),
])
def test_parse_years_legacy(start, end, expected_size):
    with tempfile.TemporaryDirectory() as tdir:
        run("parse_years", start, end, export_path=Path(tdir))
        with (Path(tdir) / f"passage_{start}_{end}.csv").open() as f:
            assert sum(1 for _ in f) == expected_size


@pytest.mark.parametrize("start,end,expected_size", [
    (2020, 2022, 20),
])
def test_build_table(start, end, expected_size):
    with tempfile.TemporaryDirectory() as tdir:
        run("build_table", export_path=Path(tdir), start=start, end=end)
        with (Path(tdir) / f"table_passage_{start}_{end}.csv").open() as f:
            assert sum(1 for _ in f) == expected_size


# on s'attend à ce que ce tests puissent ne pas passer, passés à titre informatif
@pytest.mark.xfail
@pytest.mark.parametrize("start,end", [
    # différences 2014 <-> 2015
    # - cli: modifications non présentes dans la table de référence mais présentes dans la table consolidée
    # - cli: meilleure détection de DEPCOM_2015 dans le cli
    (2014, 2015),
    # 2015 <-> 2016
    # - cli: prise en compte des cas multiples pour un même code IRIS dans le doc
    (2015, 2016),
    # 2016 <-> 2017
    # - cli: modifications consolidées vs table de référence 2017
    (2016, 2017),
    (2017, 2018),
    # 2019 <-> 2020
    # - cli: 940220104 -> 940220106 bien présent dans skl_diff et récupéré via INSEE_DOC
    (2019, 2020),
    # 2020 <-> 2021
    # - cli: 27676 -> 27058 selon le COG, pb dans nb?
    (2020, 2021),
    (2021, 2022),
])
def test_compare_cli_nb(start, end):
    """
    Compare les sorties CLI et notebook pour les millésimes traités manuellement
    """
    res = compare_cli_nb(start=start, end=end, verbose=True)
    assert res is True
