# Sources des données et documentation Insee utilisées pour le projet

## Liens site web Insee

**Définition IRIS**  
https://www.insee.fr/fr/metadonnees/definition/c1523

**Géographie de diffusion des résultats du recensement de la population**  
https://www.insee.fr/fr/information/2383182

- La géographie communale de référence
   - Zonages de diffusion des résultats du recensement de la population  
   -> https://www.insee.fr/fr/information/2440586
   - Table d’appartenance géographique des communes et tables de passage  
   -> https://www.insee.fr/fr/information/2028028
   - Modifications territoriales communales  
   -> https://www.insee.fr/fr/information/2436540


> Pour en savoir plus :  
Historique des modifications territoriales
https://www.insee.fr/fr/metadonnees/historique-commune?debut=0

- La géographie infracommunale de référence
   - Type d'IRIS  
   -> https://www.insee.fr/fr/information/2438155
   - Table d'appartenance géographique des IRIS **(données téléchargées)**  
   -> https://www.insee.fr/fr/information/2017499
   - Modifications territoriales infracommunales  
   -> https://www.insee.fr/fr/information/2434332



## IRIS

Sur la page "Table d'appartenance géographique des IRIS", un certain nombre de .zip sont disponibles.  
Ces dossiers ont été téléchargés en avril 2023.  
Les fichiers ont été copiés sur ce dépôt, dans **/data/iris** pour les données (fichiers excel), et **/docs/insee** pour les pdf de documentation.

**Liste des fichiers**  
- Tables de référence des IRIS de 2008 à 2022 : "reference_IRIS_geo20XX.xls" (ou xlsx)
- Tables de passage
    - De 2008 à 2012, par rapport à 1999 : "table_passage_IRIS_geo20XX_geo1999.xls" avec documentation pdf
    - De 2013 à 2015, par rapport à N-5 : 
        - table_passage_IRIS_geo2013_geo2008.xls
        - table_passage_IRIS_geo2014_geo2009.xls
        - table_passage_IRIS_geo2015_geo2010.xls


## Communes

Fichier copié sur ce dépôt, dans **/data/communes** :
- "table_passage_annuelle_2023.xlsx"

