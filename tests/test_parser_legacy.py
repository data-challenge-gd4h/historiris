import pandas as pd
import pytest

from historiris import parser, parser_legacy, utils

from .utils import Iris, PassageCom


def test_merge_on_lib_iris():
    df_start = pd.DataFrame([
        Iris(CODE_IRIS="i0", LIB_IRIS="Mon Iris"),
    ])
    df_end = pd.DataFrame([
        Iris(CODE_IRIS="i1", LIB_IRIS="Mon Iris"),
        Iris(CODE_IRIS="i2", LIB_IRIS="Mon (presque) Iris"),
    ])
    skl_diff = parser.compute_skl_diff(df_start, df_end)
    modifs_in_skl = parser_legacy.get_modifs_in_skl(df_end, skl_diff)
    df = parser_legacy.merge_on_lib_iris("start", "end", df_start, modifs_in_skl)
    assert df.shape[0] == 1
    assert (df.CODE_IRIS_start == "i0").all()
    assert (df.CODE_IRIS_end == "i1").all()


def test_parse_documentation(monkeypatch):
    df_start = pd.DataFrame([
        Iris(CODE_IRIS="i0"),
    ])
    df_end = pd.DataFrame([
        Iris(CODE_IRIS="i1"),
    ])
    unmatched = parser.compute_skl_diff(df_start, df_end)

    def load_modifs_mock(end, is_legacy=False):
        assert end == "end"
        return pd.DataFrame([{
            "IRIS_INI": "i0",
            "IRIS_FIN": "i1",
            "COM_INI": "d0",
            "COM_FIN": "d1",
            "annee_effet": "",
        }])

    def communes_mock(is_1999=False):
        return pd.DataFrame([
            PassageCom(CODGEO_start="d0", CODGEO_end="d0"),
        ])

    monkeypatch.setattr(utils, "load_communes_migration_table", communes_mock)
    monkeypatch.setattr(utils, "load_iris_modifications_from_compilation", load_modifs_mock)

    df = parser_legacy.parse_documentation("start", "end", df_end, unmatched)

    assert df.shape[0] == 1
    assert (df.CODE_IRIS_start == "i0").all()
    assert (df.CODE_IRIS_end == "i1").all()
    assert (df.DEPCOM_start == "d0").all()
    assert (df.DEPCOM_end == "d1").all()


def test_parse_documentation_depcom_not_valid_1999(monkeypatch):
    df_start = pd.DataFrame([
        Iris(CODE_IRIS="i0"),
    ])
    df_end = pd.DataFrame([
        Iris(CODE_IRIS="i1"),
    ])
    skl_diff = parser.compute_skl_diff(df_start, df_end)
    unmatched = parser_legacy.get_modifs_in_skl(df_end, skl_diff)

    def load_modifs_mock(end, is_legacy=False):
        assert end == "end"
        return pd.DataFrame([{
            "IRIS_INI": "i0",
            "IRIS_FIN": "i1",
            "COM_INI": "d0",
            "COM_FIN": "d1",
            "annee_effet": "",
        }])

    def communes_mock(is_1999=False):
        return pd.DataFrame([
            PassageCom(CODGEO_start="d1", CODGEO_end="d4"),
        ])

    monkeypatch.setattr(utils, "load_iris_modifications_from_compilation", load_modifs_mock)
    monkeypatch.setattr(utils, "load_communes_migration_table", communes_mock)

    with pytest.raises(AssertionError):
        parser_legacy.parse_documentation("start", "end", df_end, unmatched)
