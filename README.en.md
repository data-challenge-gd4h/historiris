# Challenge GD4H - HistorIRIS

<a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) is an initiative led by the Innovation Lab (Ecolab) of the French ministry of ecology.

A challenge has been organised in 2023 to develop tools, rooted in the health-environment data community, aiming at addressing shared issues.

<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Website</a>

## HistorIRIS

<a href="https://gd4h.ecologie.gouv.fr/defis/789" target="_blank" rel="noreferrer">En savoir plus sur le défi</a>

IRIS (Îlots Regroupés pour l’Information Statistique) are geographical divisions used by Insee for disseminating statistical data in France. They correspond to neighborhoods or portions of municipalities and are employed to generate local statistics.

Their administrative boundaries evolve over the years.

However, there is currently no correspondence table available for different vintages of IRIS.

<a href="https://gd4h.ecologie.gouv.fr/defis/789" target="_blank" rel="noreferrer">Learn more about the challenge</a>

## **Documentation**

### Data produced by the scripts

A summary table of IRIS transitions from 1999 to 2022 is available [here](/data/iris/output/table_passage_1999_2022.csv).

Detailed transition tables between successive years are also provided [here](/data/iris/output/) (except for 1999-2008). For instance, the [2010-2011 transition table](/data/iris/output/passage_2010_2011.csv).

A data paper has been produced, and it can be accessed <a href="https://hal.science/hal-04176155" target="_blank" rel="noreferrer">here</a>.

### Files consitution method

[Refer to the dedicated documentation](./docs/regles-gestions.md).

### **Installation**

[Installation Guide](/INSTALL.md)

### **Contributions**

Feedbacks on data usage can significantly contributes to enhancing the project. If you encounter an issue, please [open a ticket](https://gitlab.com/data-challenge-gd4h/historiris/-/issues/new) and provide detailed information about the context of your use. Before opening a new ticket, please check if a similar ticket is not already open.

Example of a fictitious ticket:

> Title: IRIS 163510000 does not exist in 2020
>
> Content:
> While using the transition table `data/iris/output/passage_2020_2021.csv`, I observed that IRIS 163510000 was linked to IRIS 162330000 in 2021. However, upon checking the INSEE referential for the year 2020 available [here](#), I found that IRIS 163510000 does not exist in 2020.

If you wish to contribute to this project's code, please follow the [recommendations](/CONTRIBUTING.md).

### **Licence**

The code is published under [MIT Licence](/licence.MIT).

The data referenced in this README and in the installation guide is published under [Licence Ouverte 2.0](/licence.etalab-2.0).

## Team

### Volunteers

- Alexandre
- Coline
- Bérengère
- Nadège

### Project Leaders

- Morgane
- Lucie
