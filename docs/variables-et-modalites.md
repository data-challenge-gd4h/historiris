# Variables (ou "Champs") et modalités

Sans lister de manière exhaustive toutes les variables utilisées dans les différentes tables du projet, en voici les principales.

## Issues de la documentation Insee

### Types d'IRIS

| TYP_IRIS | Description |
| --- | --- |
| H | IRIS d'habitat |
| A | IRIS d'activité |
| D | IRIS divers |
| Z | Commune non découpée en IRIS (ou "non irisée") |

> Source : https://www.insee.fr/fr/information/2438155

On distingue trois types d'IRIS :
- les IRIS d'habitat (code H) : leur population se situe en général entre 1 800 et 5 000 habitants. Ils sont homogènes quant au type d'habitat et leurs limites s'appuient sur les grandes coupures du tissu urbain (voies principales, voies ferrées, cours d'eau, ...) ;
- les IRIS d'activité (code A) : ils regroupent environ 1 000 salariés et comptent au moins deux fois plus d'emplois salariés que de population résidente ;
- les IRIS divers (code D) : il s'agit de grandes zones spécifiques peu habitées et ayant une superficie importante (parcs de loisirs, zones portuaires, forêts, ...).

Pour les communes non découpées en IRIS, le type de l'IRIS est codé à Z.

### Types de modification d'IRIS

#### Entre 1999 et 2012

>Source : Onglet "modifications_iris" du fichier "table_passage_IRIS_geo2012_geo1999.xls"

| MODIF_IRIS | Libellé de la modification |
| --- | --- |
| 1Z | Scission d'IRIS |
| 2A à 2AC | Déplacement de limites d'IRIS (du groupe 2A au groupe 2AC) |
| 3A | Fusion de communes irisées |
| 3B | Rétablissement de communes issues de communes irisées |
| 4A | Échange de parcelles entre communes irisées |
| 4B | Échange de parcelles habitées entre communes irisées et non irisées |
| 4C | Échange de parcelles inhabitées entre communes irisées et non irisées |

#### Notes

Plus de détails et exemples ici : [Documentation Insee de la table de passage 1999-2008](/docs/insee/DOC_table_passage_IRIS_geo2008_geo1999.md)

**Modification de type 2**

>- Le code « 2 » en première position du code de modification permet de repérer les IRIS ayant vu leurs limites géographiques se déplacer.
>- Pour comparer les résultats du recensement de 2006 avec ceux du recensement de 1999, il convient de regrouper, à la fois en 2006 et en 1999, les IRIS concernés par un déplacement de limites.
>- La lettre en deuxième position du code de modification permet de repérer les regroupements à effectuer.

Logiquement, quand un nouveau déplacement de limite arrive, une nouvelle modalité est ajoutée (incrémentaiton de la partie en lettres).

Ainsi, en 2008, les codes de type 2 s'arrêtaient à "W", et les codes "2X" à "2AC" ont été ajoutés entre 2009 et 2012.
>Onglet "code_modification" du fichier "table_passage_IRIS_geo2008_geo1999.xls" :
>| Code_modification | Libellé de la modification |
>| --- | --- |
>| 2A à 2W | Déplacement de limites d'IRIS (du groupe 2A au groupe 2W) |

#### Depuis 2012

| MODIF_IRIS | NATURE_MODIF |
| --- | --- |
| O | pas de modification (ou commune non irisée) |
| 2 | Déplacement de limites |
| 3 | Rétablissement/Fusion de communes irisées |
| 4 | Échange de parcelles |

>Source : Onglet " Documentation du fichier "reference_IRIS_geo2022.xlsx"

Les différentes modifications d'IRIS survenues sur une période de cinq ans sont identifiées grâce au code modification de l'IRIS qui se décline selon les modalités suivantes :
        0 pas de modification (ou commune non irisée),
        2 déplacement de limites d'IRIS,
        3 rétablissement ou fusion de communes issues de communes irisées,
        4 échange de parcelles entre communes irisées.


"Les modifications d’IRIS sont renseignées uniquement pour les communes irisées. Pour les communes non irisées, se référer à la table de passage annuelle 2022 pour obtenir les modifications de communes intervenues entre 2003 et 2022.

Les échanges de parcelles donnent lieu à une renumérotation des iris concernés quand ils sont considérés comme significatifs, par le nombre de logements affectés. Les autres échanges de parcelles sont indiqués à titre informatif, sans garantie d'exhaustivité.

En 2022, seul l'échange de parcelles entre les communes irisées de Jouy-en-Josas (78322) et de Versailles (78646) est considéré comme significatif. Les deux iris concernés font donc l'objet d'une renumérotation : l'iris 783220101 devient 783220105, et l'iris 786460307 devient 786460308."
https://www.insee.fr/fr/information/2028028

Rappel : le code IRIS fictif attribué à une commune non irisée est de la forme XXXXX0000, où XXXXX est le code Insee de la commune.


## Créées pour le projet HistorIRIS

[Voir la documentation des méthodes de réconciliation](../docs/regles-gestions.md) pour plus de détails.

- `HH_FUSION_SCISSION`, Changement de code IRIS réconcilié sur le libellé de l'IRIS : jusqu'en 2012, correspond à la réconciliation sur le libellé de l'IRIS.
- `HH_FUSION_IRISEE`, Fusion de communes irisées : non utilisé à date.
- `HH_FUSION`, Fusion de communes non irisées : réconciliation sur la base du fichier de passage des communes
- `HH_CREATION`, Nouvelle commune : détection de création d'une nouvelle commune et d'un nouvel IRIS
