> ℹ Fichier d'origine (Insee) : document pdf, associé au fichier excel *table_passage_IRIS_geo2008_geo1999.xls* (voir dossier data/iris)  
> En voici une version markdown.

# Passage des IRIS en géographie 2008 aux IRIS en géographie 1999

Les communes de 10 000 habitants ou plus et la plupart des communes de 5 000 à 9 999 habitants ont été découpées en IRIS pour la diffusion des résultats du recensement de 1999.

Depuis, le découpage en IRIS n'a été modifié qu'à la marge. D’une part, chaque année, il a été adapté pour tenir compte des modifications des limites communales. D’autre part, une retouche très partielle est intervenue, en 2008, pour prendre en compte les évolutions importantes de la voierie et de la démographie. Cette retouche a été réalisée en respectant les règles établies en 1999 avec la Commission Nationale de l'Informatique et des Libertés (CNIL) et en préservant au maximum la continuité des séries de diffusion. Elle a été limitée à une centaine d’IRIS afin de préserver la continuité des séries de diffusion.

Les IRIS dans le découpage de 1999 sont appelés « IRIS en géographie 1999 » et les IRIS dans le découpage de 2008 sont appelés « IRIS en géographie 2008 ».

**IMPORTANT**  
Le découpage en IRIS de 1999 est resté très stable. Seuls 187 IRIS sur les 15 375 existants en géographie 1999 en France métropolitaine [1] ont participé à un des mouvements décrits ci-dessous. En géographie 2008, ces mouvements ont concerné 245 IRIS sur les 15 432 existants au 1er janvier 2008 en France métropolitaine. La liste des communes et des départements concernés par ces quelques mouvements est fournie dans l’onglet « recap_departements_communes » de la table de passage.

[1] La seule modification apportée aux IRIS des DOM concerne un changement de libellé d’un IRIS (971060103- Pigeon-Malendure-Caféière).

## 1. Une retouche partielle du découpage en 2008

L'Insee a effectué, au cours du deuxième trimestre 2008, une retouche très partielle du découpage en IRIS consistant à scinder des IRIS en 2 ou 3 pour tenir compte des évolutions démographiques importantes et à déplacer les limites de quelques IRIS lorsque des modifications de la voierie l’imposaient.

### 1.1. Les scissions d’IRIS

Il a été procédé à 50 scissions d’IRIS ; 46 IRIS en géographie 1999 ont été scindés en deux IRIS en géographie 2008 et 4 IRIS 1999 l’ont été en 3 IRIS 2008. Au total, 104 IRIS en géographie 2008 résultent de scissions d'IRIS de 1999. Ils sont repérables dans la table de passage par la modalité ‘1Z’ du code modification. Il est donc possible de reconstituer l’IRIS en géographie 1999 grâce à cette table.

**Exemple**  
L’IRIS ‘0129’ de la commune de Cannes (06029) a été scindé en deux IRIS : ‘0131’ et ‘0132’. La table fait correspondre à ces deux IRIS le code IRIS 1999 :

| IRIS_GEO2008 | IRIS_GEO1999 | code_modification |
| --- | --- | --- |
| 60290131 | 60290129 | 1Z |
| 60290132 | 60290129 | 1Z |

### 1.2. Les déplacements des limites d’IRIS

En 2008, 23 déplacements de limite d’IRIS ont été introduits. Dans 20 cas, 2 IRIS étaient concernés, dans 1 cas 3 IRIS étaient concernés et dans 1 cas 6 IRIS l’étaient. Au total, 52 IRIS ont vu leurs limites se déplacer. Le code « 2 » en première position du code de modification permet de repérer les IRIS ayant vu leurs limites géographiques se déplacer.

Pour comparer les résultats du recensement de 2006 avec ceux du recensement de 1999, il convient de regrouper, à la fois en 2006 et en 1999, les IRIS concernés par un déplacement de limites.

La lettre en deuxième position du code de modification permet de repérer les regroupements à effectuer.

**Exemple**  
Dans la commune de Narbonne (11262), la limite entre les IRIS ‘0204’ et ‘0205’ a été déplacée. Les nouveaux IRIS sont renumérotés ‘0206’ et ‘0207’. Pour comparer 2006 à 1999, il faut d’une part regrouper les IRIS ‘0204’ et ‘0205’ et d’autre part les IRIS ‘0206’ et ‘0207’. Les deux regroupements d’IRIS ont les mêmes limites et sont comparables.

Dans l’onglet « passage_geo2008versgeo1999 », l’information est présentée comme suit :

| IRIS_GEO2008 | IRIS_GEO1999 | code_modification |
| --- | --- | --- |
| 112620206 | 112620204 | 2B |
| 112620206 | 112620205 | 2B |
| 112620207 | 112620205 | 2B |

Cela signifie que l’IRIS ‘0204’ et une partie de l’IRIS ‘0205’ sont devenus l’IRIS ‘0206’. La partie restante de l’IRIS ‘0205’ devient l’IRIS ‘0207’. Il est donc nécessaire de décrire ce mouvement sur trois enregistrements.

Ici, la lettre « B » du code de modification permet de savoir que ces trois enregistrements sont à regrouper. Ainsi, les données des IRIS «0204 » additionnées aux données de l’IRIS « 0205 » sont comparables aux données des IRIS « 0206 » additionnées aux données de l’IRIS « 0207 ».

## 2. Les mouvements communaux

Au-delà de cette retouche, différents évènements touchant les communes ont pu entraîner une modification de la géographie infra-communale.

### 2.1. Les fusions de communes irisées

Du 8 mars 1999 au 1er janvier 2008 inclus, 2 fusions de communes irisées ont eu lieu :
• fusion de Cherbourg (50129) et d’Octeville (50383) pour donner la commune de Cherbourg-Octeville ;
• fusion de Lille (59350) et Lomme (59355) pour donner Lille.

Les IRIS d’Octeville sont donc devenus des nouveaux IRIS de Cherbourg-Octeville. De la même manière, les IRIS de Lomme sont devenus des nouveaux IRIS de Lille. Les correspondances entre ces IRIS sont repérables dans la table de passage grâce au code 3A. Dans ces deux cas, les populations des IRIS en géographie 1999 ou 2008 sont comparables grâce à ces correspondances.

Pour information, les fusions entre communes non irisées font partie des évènements décrits dans l’onglet « modifications_communales ».

### 2.2. Les rétablissements de communes

Du 8 mars 1999 au 1er janvier 2008 inclus, 3 communes ont été rétablies qui antérieurement à cette période avaient fusionné : séparation de Béthune (62 119) et de Verquigneul (62 847) ; séparation de Tournus (71 543) et de Plottes (71353) et séparation de Mauvezin-sur-Gupie (47163) et de Marmande (47157).

Les IRIS concernés sont repérables dans la table de passage par la modalité ‘3B’ du code modification.

Il est possible de comparer les populations en restant à l’échelle des IRIS.

Pour information, les rétablissements des communes ne faisant intervenir que des communes non irisées font partie des évènements décrits dans l’onglet « modifications_communales ».

### 2.3. Les échanges de parcelles

Du 8 mars 1999 au 1er janvier 2008 inclus, 43 communes ont connu des échanges de parcelles entre elles ou des restructurations internes sur leur propre territoire. Ces échanges se traduisent par des modifications de contours des IRIS pour les communes irisées. Une parcelle correspond à une parcelle cadastrale.

La table de passage distingue trois types d’échanges de parcelles :
- les échanges de parcelles entre communes irisées (45 IRIS en géographie 2008 concernés), codés 4A ;
- les échanges de parcelles habitées entre communes irisées et communes non irisées (10 IRIS en géographie 2008 concernés), codés 4B ;
- les échanges de parcelles inhabitées entre communes irisées et communes non irisées (7 IRIS en géographie 2008 concernés), codés 4C.

Dans le cas d’échanges de parcelles inhabitées, les comparaisons démographiques restent possibles, même si le territoire a changé de contour.

**Exemples d’échanges codés 4A**  
a) L’échange de parcelles entre Les Ponts-de-Cé (49246) et Trélazé (49353) entraîne la création de l’IRIS 493530110 correspondant à une partie de l’IRIS 492460104 jointe à l’IRIS 493530103. La partie restante de l’IRIS 492460104 devient l’IRIS 492460110.

| IRIS_GEO2008 | IRIS_GEO1999 | code_modification |
| --- | --- | --- |
| 492460110 | 492460104 | 4A |
| 493530110 | 493530103 | 4A |
| 493530110 | 492460104 | 4A |


b) La commune de Nanterre (92050) a restructuré la plupart de ses IRIS, ainsi la table de passage fournit 90 mouvements d’IRIS pour décrire cette restructuration qui aboutit à une nouvelle partition de la commune en 10 IRIS en géographie 2008. Les populations impliquées dans ces mouvements ne peuvent être reconstituées à l’échelle des IRIS.

**Exemple d’échange codé 4B**  
Une parcelle de la commune de Léhon (22123), notée « 221230000(p) » comptant 218 habitants au recensement de 1999 est rattachée à l’IRIS 2205000105 de la commune de Dinan (22050). Cet IRIS prend alors le numéro 2205000107.

| IRIS_GEO2008 | IRIS_GEO1999 | code_modification |
| --- | --- | --- |
| 220500107 | 220500105 | 4B |
| 220500107 | 221230000(p) | 4B |

**Exemple d’échange codé 4C**  
Une parcelle de la commune de Saint-Vigor-le-Grand (14663) sans habitants est rattachée à l’IRIS 140450202 de la commune de Bayeux (14047). Cet IRIS prend alors le numéro 140470203.

| IRIS_GEO2008 | IRIS_GEO1999 | code_modification |
| --- | --- | --- |
| 140470203 | 140470202 | 4C |
| 140470203 | 146630000(p) | 4C |

## 3. Le contenu de la table de passage

Les mouvements décrits aux points 1 et 2 précédents sont tous présents dans l’onglet « passage_geo2008versgeo1999 ». Les identifiants IRIS en géographie 2008 sont en première colonne et sont triés par ordre croissant.

L’onglet « passage_geo1999versgeo2008 » permet d’avoir la vision de ces mêmes événements mais en partant de l’existant en 1999. Les identifiants des IRIS en géographie 1999 sont en première colonne et sont triés par ordre croissant.

L’onglet « code_modification » donne la correspondance entre le code identifiant le mouvement et son libellé.

L’onglet « recap_departements_communes » fournit la liste des 43 départements et de la centaine de communes ayant connu une modification d’IRIS entre la géographie de 1999 et celle de 2008.

L’onglet « modifications_communales » fournit la liste des mouvements géographiques des communes qui se sont produits entre le 1er janvier 2000 et le 1er janvier 2008.

> Mise à jour le 16 octobre 2012