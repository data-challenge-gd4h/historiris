# Guide d'architecture et d'installation

## Architecture

```console
.
├── CONTRIBUTING.md
├── INSTALL.md
├── README.en.md
├── README.md
├── data                            # données brutes
│   ├── communes                    #   relatives aux communes
│   └── iris                        #   relatives aux IRIS
│       └── output                  #   de sortie du script du traitement
├── docs                            # documentation
│   ├── insee                       #   issue de l'INSEE
│   ├── regles-gestions.md          #   du traitement
│   ├── sources.md                  #   des sources de données
│   └── variables-et-modalites.md   #   des variables et de leurs modalités
├── historiris                      # code source du projet
├── licence.MIT
├── licence.etalab-2.0
├── notebooks                       # notebooks d'exploration et de visualisation des données
│   └── output                      # données de sortie des notebooks
├── requirements-notebooks.txt      # dépendances pour faire tourner les notebooks
├── requirements-tests.txt          # dépendances pour faire tourner les tests
├── requirements.txt                # dépendances générales
├── setup.cfg                       # fichier de configuration du package Python
├── setup.py
└── tests                           # dossier contenant les fichiers de tests
```

## Installation

Le projet nécessite une installation fonctionnelle de python3, de préférence >=3.10.

On crée d'abord un environnement virtuel (venv) pour installer les dépendances Python confortablement :

```console
$ python3 -mvenv pyenv
```

Cet environnement devra être activé avant toute commande manipulant les librairies Python du projet : installation, script, tests... :

```console
$ source pyenv/bin/activate
```

Dans les commandes suivantes, le _prompt_ `$(pyenv)` indique que l'environnement virtuel est activé.

Installation ou mise à jour des dépendances pour un développement en local :

```console
$(pyenv) pip install -e .
```

Installation ou mise à jour des dépendances pour un développement en local avec la partie `notebooks` :

```console
$(pyenv) pip install -e .[notebooks]
```

Installation ou mise à jour des dépendances pour un développement en local avec la partie `tests` :

```console
$(pyenv) pip install -e .[tests]
```

On vérifie que tout est bon :

```console
$(pyenv) pytest
=================================== test session starts ====================================
platform darwin -- Python 3.11.3, pytest-7.3.1, pluggy-1.0.0
rootdir: /Users/alexandre/Developer/HistorIRIS/project/historiris
collected 1 item

tests/test_historiris.py .                                                           [100%]

==================================== 1 passed in 0.12s =====================================
```

>Pour windows, adapter les commandes d'après : https://docs.python.org/fr/3/library/venv.html

## Utilisation

### Configuration

Le fichier `historiris/settings.toml` porte la configuration relative aux différents millésimes.

En cas d'ajout de millésime, il est nécessaire d'ajuster cette configuration :
- référence au fichier source
- documentation des exceptions sur ce millésime le cas échéant

### Génération d'une table de passage complète bisannuelle

Ex : table de passage de 2021 à 2022.

```console
$(pyenv) historiris parse-years -v 2021 2022
```

La sortie est dans le fichier `data/iris/output/passage_2021_2022.csv`.

### Génération d'une table de passage simplifiée pluriannuelle

Ex : table de passage de 2012 à 2022.

```console
$(pyenv) historiris build-table -v --start 2012 --end 2022
```

L'option `--force-refresh` permet de forcer la génération des tables bisannuelles sous-jacentes (cf plus haut).

La sortie est dans le fichier `data/iris/output/table_passage_2012_2022.csv`.
