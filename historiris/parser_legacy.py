import logging

import pandas as pd

from historiris import utils

log = logging.getLogger(__name__)


def get_modifs_in_skl(df_end: pd.DataFrame, skl_diff: pd.DataFrame) -> pd.DataFrame:
    """
    Récupère les IRIS de skl_diff réputés modifiés dans df_end
    """
    #
    modifs = df_end[df_end["MODIF_IRIS"] != "00"]
    modifs_in_skl = modifs[modifs["CODE_IRIS"].isin(skl_diff.CODE_IRIS)]
    log.info(f"{modifs_in_skl.shape[0]} modifications détectées via MODIF_IRIS")
    log.debug(f"modifs_in_skl\n{modifs_in_skl}")
    return modifs_in_skl


def merge_on_lib_iris(start: str, end: str, df_start: pd.DataFrame, modifs_in_skl: pd.DataFrame):
    """
    Construit une table de passage via le merge de `modifs_in_skl` et `df_start` sur (LIB_IRIS, DEPCOM)
    """
    if "LIB_IRIS" not in df_start.columns:
        log.warning("Pas de LIB_IRIS trouvé dans df_start, on saute merge_on_lib_iris")
        return pd.DataFrame(columns=[f"CODE_IRIS_{start}", f"CODE_IRIS_{end}"])

    cols = ["CODE_IRIS", "LIB_IRIS", "DEPCOM"]
    df_passage_lib_iris = modifs_in_skl[cols + ["TYP_IRIS"]].merge(
        df_start[cols], on=["LIB_IRIS", "DEPCOM"], suffixes=[f"_{end}", f"_{start}"]
    )
    df_passage_lib_iris = df_passage_lib_iris.drop(columns="LIB_IRIS")
    df_passage_lib_iris = df_passage_lib_iris.assign(**{
        f"DEPCOM_{start}": df_passage_lib_iris["DEPCOM"],
        "SOURCE": "HH_MERGE_LIB_IRIS",
        "ANNEE_MODIF": end,
        "MODIF_IRIS": "HH_FUSION_SCISSION",
        "NATURE_MODIF": "Changement de code IRIS réconcilié sur le libellé de l'IRIS",
    })
    df_passage_lib_iris = df_passage_lib_iris.rename(columns={"DEPCOM": f"DEPCOM_{end}"})
    log.debug(f"df_passage_lib_iris\n{df_passage_lib_iris}")

    return df_passage_lib_iris


def parse_documentation(start: str, end: str, df_end: pd.DataFrame, unmatched: pd.DataFrame) -> pd.DataFrame:
    """
    Crée un table de passage en réconciliant les modifications documentées pour l'année `end`
    et les cas encore inexpliqués présents dans `unmatched`.
    """
    # réconciliation unmatched vs modifications documentées pour start
    modifs_doc = utils.load_iris_modifications_from_compilation(str(end), is_legacy=True)
    unmatched_isin_modifs = modifs_doc[modifs_doc["IRIS_FIN"].isin(unmatched.CODE_IRIS)]
    # vérification de la validité du DEPCOM 1999 sur l'année start si besoin
    if start != "1999":
        df_com = utils.load_communes_migration_table()
        assert unmatched_isin_modifs.COM_INI.isin(df_com[f"CODGEO_{start}"]).all(), \
            f"DEPCOM 1999 non valable sur {start}"
    # construction de la table de passage doc
    df_passage_doc = unmatched_isin_modifs.drop(columns=["annee_effet"])
    # on enlève le suffixe (p) le cas échéant (échange de parcelles)
    df_passage_doc.IRIS_INI = df_passage_doc.IRIS_INI.str.replace("(p)", "")
    df_passage_doc = df_passage_doc.rename(columns={
        "IRIS_FIN": f"CODE_IRIS_{end}",
        "IRIS_INI": f"CODE_IRIS_{start}",
        "COM_INI": f"DEPCOM_{start}",
        "COM_FIN": f"DEPCOM_{end}",
    })
    df_passage_doc = df_passage_doc.assign(**{
        "SOURCE": "INSEE_DOC_LEGACY",
    })

    # on récupère le `TYP_IRIS` depuis la source `end`
    # *ATTENTION* : ce merge supprime les lignes qui n'ont pas de correspondance sur le CODE_IRIS_end dans `df_end`
    cols = ["CODE_IRIS", "TYP_IRIS"]
    df_passage_doc = df_passage_doc.merge(
        df_end[cols], left_on=f"CODE_IRIS_{end}", right_on="CODE_IRIS", validate="m:1",
    )
    df_passage_doc = df_passage_doc.drop(columns=["CODE_IRIS"])

    log.debug(f"df_passage_doc\n{df_passage_doc}")

    return df_passage_doc
