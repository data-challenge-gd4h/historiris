import toml

from pathlib import Path

from dotmap import DotMap


settings = DotMap(toml.load(Path(__file__).parent / "settings.toml"))
