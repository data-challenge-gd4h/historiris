import logging

import pandas as pd
import pytest

from historiris import parser, utils
from historiris.settings import settings

from .utils import Iris, PassageCom


@pytest.fixture(autouse=True)
def setup_logging(caplog):
    caplog.set_level(logging.DEBUG)


def test_compare_common_lines(caplog, monkeypatch):
    df_start = pd.DataFrame({
        "CODE_IRIS": ["w", "x", "y"],
        "AUTRE": ["", "a", "b"]
    })
    df_end = pd.DataFrame({
        "CODE_IRIS": ["x", "y", "z"],
        "AUTRE": ["a", "f", ""],
        "END_ONLY": ["a", "b", ""],
    })

    # on vérifie que le traitement s'arrête sur les colonnes
    # - avec des valeurs différentes
    # - présentes seulement d'un côté
    with pytest.raises(SystemExit):
        parser.compare_common_lines(df_start, df_end, "start_end")
    assert any(
        "Différence non gérée sur la colonne" in r.msg for r in caplog.records
    )
    assert any("END_ONLY" in r.msg for r in caplog.records)
    assert any("END_ONLY" in r.msg for r in caplog.records)

    # on documente les changements sur les colonnes
    monkeypatch.setitem(settings, "IGNORED_DIFF_COLS_YEARS", {
        "start_end": ["END_ONLY", "AUTRE"]
    })
    # le traitement passe maintenant sans problème
    parser.compare_common_lines(df_start, df_end, "start_end")


def test_compute_skl_diff():
    df_start = pd.DataFrame([
        Iris(CODE_IRIS="i0", DEPCOM="d0"),
        Iris(CODE_IRIS="i1", DEPCOM="d1"),
        Iris(CODE_IRIS="i2", DEPCOM="d2"),
        Iris(CODE_IRIS="i3", DEPCOM="d3"),
    ])
    df_end = pd.DataFrame([
        Iris(CODE_IRIS="i1", DEPCOM="d1"),
        Iris(CODE_IRIS="i2", DEPCOM="d2"),
        Iris(CODE_IRIS="i3", DEPCOM="d3"),
        Iris(CODE_IRIS="i4", DEPCOM="d4"),
    ])

    skl_diff = parser.compute_skl_diff(df_start, df_end)

    # deux lignes, une pour i4 et une pour i0
    assert skl_diff.shape[0] == 2
    # i4 est dans end et pas dans start
    assert skl_diff[skl_diff["CODE_IRIS"] == "i4"]["DEPCOM_start"].isna().all()
    assert (skl_diff[skl_diff["CODE_IRIS"] == "i4"]["DEPCOM_end"] == "d4").all()
    # i0 est dans start et pas dans end
    assert (skl_diff[skl_diff["CODE_IRIS"] == "i0"]["DEPCOM_start"] == "d0").all()
    assert skl_diff[skl_diff["CODE_IRIS"] == "i0"]["DEPCOM_end"].isna().all()


def test_get_modifications():
    modifs = parser.get_modifications("2022")
    assert modifs.shape[0] == 5

    with pytest.raises(AssertionError):
        # TODO: update in 2041 :p
        parser.get_modifications("2042")


def test_parse_documentation(monkeypatch, caplog):
    df_start = pd.DataFrame([
        Iris(CODE_IRIS="i0", DEPCOM="d0", TYP_IRIS="Z"),
        Iris(CODE_IRIS="i1", DEPCOM="d1", TYP_IRIS="Z"),
        Iris(CODE_IRIS="i2", DEPCOM="d2", TYP_IRIS="Z"),
        Iris(CODE_IRIS="i3", DEPCOM="d3", TYP_IRIS="Z"),
    ])
    df_end = pd.DataFrame([
        Iris(CODE_IRIS="i1", DEPCOM="d1", TYP_IRIS="Z"),
        Iris(CODE_IRIS="i2", DEPCOM="d2", TYP_IRIS="Z"),
        Iris(CODE_IRIS="i3", DEPCOM="d3", TYP_IRIS="Z"),
        Iris(CODE_IRIS="i4", DEPCOM="d4", TYP_IRIS="Z"),
        Iris(CODE_IRIS="iris_fin", DEPCOM="depcom_fin", TYP_IRIS="Z_fin"),
    ])

    skl_diff = parser.compute_skl_diff(df_start, df_end)

    def modifs_mock(_):
        return pd.DataFrame({
            "IRIS_INI":     ["iris_ini", "i0", "autre_iris_ini"],
            "IRIS_FIN":     ["i4", "iris_fin", "autre_iris_fin"],
            "NATURE_MODIF": ["nature_modif", "nature_modif", "xxx"],
            "COM_INI":      ["com_ini", "com_ini", "xxx"],
            "COM_FIN":      ["com_fin", "com_fin", "xxx"],
            "ANNEE_MODIF":  ["annee_modif", "annee_modif", "xxx"],
            "MODIF_IRIS":   ["modif_iris", "modif_iris", "xxx"],
        })

    monkeypatch.setattr(parser, "get_modifications", modifs_mock)

    df_doc = parser.parse_documentation("start", "end", df_end, skl_diff)
    assert df_doc.shape[0] == 2

    # la documentation associe i4 à iris_ini sur com_ini, on doit retrouver cette modification
    assert (df_doc[df_doc["CODE_IRIS_end"] == "i4"]["CODE_IRIS_start"] == "iris_ini").all()
    assert (df_doc[df_doc["CODE_IRIS_end"] == "i4"]["DEPCOM_start"] == "com_ini").all()
    assert (df_doc[df_doc["CODE_IRIS_end"] == "i4"]["TYP_IRIS"] == "Z").all()

    # la documentation associe i0 à iris_fin sur com_fin, on doit retrouver cette modification
    assert (df_doc[df_doc["CODE_IRIS_start"] == "i0"]["CODE_IRIS_end"] == "iris_fin").all()
    assert (df_doc[df_doc["CODE_IRIS_start"] == "i0"]["DEPCOM_end"] == "com_fin").all()
    assert (df_doc[df_doc["CODE_IRIS_end"] == "i0"]["TYP_IRIS"] == "Z_fin").all()

    # autre_iris_ini / autre_iris_fin lève un warning car pas présent dans skl_diff
    assert any(
        "modifications documentées impliquant un changement d'IRIS n'ont pas été rattachées aux différences détectées"
        in r.msg for r in caplog.records
    )


def test_parse_cog(monkeypatch):
    df_start = pd.DataFrame([
        Iris(CODE_IRIS="i0", DEPCOM="d0", TYP_IRIS="Z"),
        Iris(CODE_IRIS="i1", DEPCOM="d1", TYP_IRIS="Z"),
    ])
    df_end = pd.DataFrame([
        Iris(CODE_IRIS="i1", DEPCOM="d1", TYP_IRIS="Z"),
        Iris(CODE_IRIS="i4", DEPCOM="d4", TYP_IRIS="Z"),
    ])

    def communes_mock(is_1999=False):
        return pd.DataFrame([
            PassageCom(CODGEO_start="d1", CODGEO_end="d4"),
            # inutilisé car d2 inutilisé
            PassageCom(CODGEO_start="d0", CODGEO_end="d2"),
        ])

    monkeypatch.setattr(utils, "load_communes_migration_table", communes_mock)

    # on fait comme si la documentation n'avait rien expliqué
    unexplained = parser.compute_skl_diff(df_start, df_end)
    df_com = parser.parse_cog("start", "end", df_start, df_end, unexplained)

    assert df_com.shape[0] == 1

    # mapping i4 -> i1 via d4 -> d1 (DEPCOM_end)
    assert (df_com[df_com["CODE_IRIS_end"] == "i4"]["CODE_IRIS_start"] == "i1").all()
    assert (df_com[df_com["CODE_IRIS_end"] == "i4"]["SOURCE"] == "HH_INSEE_COG").all()
    assert (df_com[df_com["CODE_IRIS_end"] == "i4"]["MODIF_IRIS"] == "HH_FUSION").all()


def test_parse_cog_non_irisees_com_end(monkeypatch, caplog):
    df_start = pd.DataFrame([
        Iris(CODE_IRIS="i0", DEPCOM="d0", TYP_IRIS="Z"),
    ])
    df_end = pd.DataFrame([
        Iris(CODE_IRIS="i4", DEPCOM="d4", TYP_IRIS="H", MODIF_IRIS="1"),
        Iris(CODE_IRIS="i4bis", DEPCOM="d4", TYP_IRIS="H"),
    ])

    def communes_mock(is_1999=False):
        return pd.DataFrame([
            PassageCom(CODGEO_start="d0", CODGEO_end="d4"),
        ])

    monkeypatch.setattr(utils, "load_communes_migration_table", communes_mock)

    unexplained = parser.compute_skl_diff(df_start, df_end)
    with pytest.raises(AssertionError):
        parser.parse_cog("start", "end", df_start, df_end, unexplained)


def test_handle_unexplained():
    df_start = pd.DataFrame({
        "CODE_IRIS":    [],
        "DEPCOM":       [],
        "TYP_IRIS":     [],
    })
    df_end = pd.DataFrame({
        "CODE_IRIS":    ["i2", "i5"],
        "DEPCOM":       ["d2", "d6"],
        "TYP_IRIS":     ["Z",   "H"],
    })

    # on fait comme si la documentation n'avait rien expliqué
    unexplained_left = parser.compute_skl_diff(df_start, df_end)
    df_unexplained = parser.handle_unexplained("start", "end", df_end, unexplained_left)

    assert df_unexplained.shape[0] == 2

    assert (df_unexplained["MODIF_IRIS"] == "HH_CREATION").all()
    assert (df_unexplained["CODE_IRIS_start"].isna()).all()
    assert "i2" in df_unexplained.CODE_IRIS_end.to_list()
    assert "i5" in df_unexplained.CODE_IRIS_end.to_list()
