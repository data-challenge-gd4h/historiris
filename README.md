# Challenge GD4H - HistorIRIS

Le <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) est une offre de service incubée au sein de l’ECOLAB, laboratoire d’innovation pour la transition écologique du Commissariat Général au Développement Durable.

Dans ce cadre, un challenge permettant le développement d’outils ancrés dans la communauté de la donnée en santé-environnement afin d’adresser des problématiques partagées a été organisé en 2023.

<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Site</a>


## HistorIRIS

Les IRIS (Îlots Regroupés pour l’Information Statistique) sont des découpages géographiques utilisés par l’Insee pour la diffusion de données statistiques en France. Ils correspondent à des quartiers ou portions de communes et sont utilisés pour produire des statistiques locales.

Leurs contours administratifs évoluent au fil des années.

Or, il n’existe pas aujourd’hui de table de correspondance entre les différents millésimes des IRIS.

<a href="https://gd4h.ecologie.gouv.fr/defis/789" target="_blank" rel="noreferrer">En savoir plus sur le défi</a>

## **Documentation**

### Données produites par les scripts

Une table de passage synthétique des IRIS de 1999 à 2022 est [disponible ici](/data/iris/output/table_passage_1999_2022.csv).

Il existe également des [tables de passages détaillées entre deux millésimes successifs](/data/iris/output/) (sauf 1999-2008). Par exemple la [table de passage 2010-2011](/data/iris/output/passage_2010_2011.csv).

Un data paper a été produit, il est accessible <a href="https://hal.science/hal-04176155" target="_blank" rel="noreferrer">ici</a>.

### Méthode de constitution des fichiers

[Voir la documentation dédiée](./docs/regles-gestions.md)

### **Installation et utilisation**

[Guide d'installation](/INSTALL.md)

### **Contributions**

Les retours sur l'utilisation des données sont très utiles pour l'amélioration du projet. Si vous rencontrez un problème, merci d'[ouvrir un ticket](https://gitlab.com/data-challenge-gd4h/historiris/-/issues/new) en expliquant en détails le contexte de votre utilisation. Merci de vérifier au préalable [si un ticket similaire n'est pas déjà ouvert](https://gitlab.com/data-challenge-gd4h/historiris/-/issues).

Ex de ticket fictif :

> Titre : IRIS 163510000 n'existe pas en 2020
>
> Contenu :
> En utilisant la table de passage `data/iris/output/passage_2020_2021.csv`, j'ai constaté que l'IRIS 163510000 était rattaché à l'IRIS 162330000 en 2021. Or en regardant dans le référentiel de l'INSEE pour l'année 2020 [disponible ici](#), l'IRIS 163510000 n'existe pas en 2020.

Si vous souhaitez contribuer au code de ce projet, merci de suivre les [recommendations](/CONTRIBUTING.md).

### **Licence**

Le code est publié sous licence [MIT](/licence.MIT).

Les données référencés dans ce README et dans le guide d'installation sont publiés sous [Licence Ouverte 2.0](/licence.etalab-2.0).

## Equipe

### Bénévoles

- Alexandre
- Coline
- Bérengère
- Nadège

### Porteuses du projet

- Morgane
- Lucie
