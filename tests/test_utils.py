import pytest

from historiris.utils import load_iris_list, load_iris_modifications, load_communes_migration_table


@pytest.mark.parametrize("year,extension,expected_size", [
    (2008, "xls", 50881),
    (2014, "xls", 50866),
    (2015, "xls", 50843),
    (2016, "xls", 50218),
    (2021, "xlsx", 49406),
    (2022, "xlsx", 49397),
])
def test_load_iris_list(year, extension, expected_size):
    df = load_iris_list(year, extension=extension)
    assert df.shape[0] == expected_size


@pytest.mark.parametrize("year,extension,expected_size", [
    (2008, "xls", None),
    (2014, "xls", None),
    (2015, "xls", None),
    (2016, "xls", 135),
    (2021, "xlsx", 322),
    (2022, "xlsx", 145)
])
def test_load_iris_modifications(year, extension, expected_size):
    # expected_size is None correspond à des millésimes sans documentation des modifications
    if expected_size is not None:
        df = load_iris_modifications(year, extension=extension)
        assert df.shape[0] == expected_size


def test_load_communes_migration_table():
    df = load_communes_migration_table()
    assert df.shape[0] == 36763
