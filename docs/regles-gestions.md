# Règles de gestion

Ce document liste les sources, la méthode et les différents cas rencontrés lors de la constitution du référentiel final (table de passage pluriannuelle des IRIS).

## Sources

[Liste des sources](/docs/sources.md)

## Méthode générale

1. Création de tables de passage bisannuelles (1999-2008, 2008-2009, ..., 2014-2015, 2015-2016...)
2. Rassemblement des données de ces tables en une table de passage synthétique complète (1999-2022)

## Table de passage bisannuelle

Par exemple pour 2020-2021 : `historiris parse-years -v 2020 2021`. Dans ce cas on considère qu'on traite l'année 2021 (année N).

### Méthodes communes à tous les millésimes

1. Chargement des fichiers de références (liste des IRIS) pour les années N et N-1.
2. Comparaison des lignes communes : pour chaque code IRIS, on compare les valeurs des colonnes associées sur N et N-1. On ignore `LIB_IRIS` et `LIBCOM` pour toutes les années et certaines autres colonnes en fonction des années, voir `IGNORED_DIFF_COLS_YEARS` dans `settings.toml`. Si une différence n'est pas explicitement ignorée, le script s'arrête.
3. Calcul des différences entre les millésimes en utilisant le couple (CODE_IRIS, CODE_INSEE), ce qui permet d'ignorer les valeurs différentes identifiées plus haut. On obtient ce qu'on appelle un _diff squelette_.
4. **Etapes spécifiques au millésime, voir plus bas**
5. Chargement du [fichier de passage des communes de l'INSEE à partir de 2003](../data/communes/table_passage_annuelle_2023.xlsx) ou le [fichier calculé spécifique pour 1999-2008](../data/communes/correspondance_1999_2008.csv).
6. Création d'une table de passage intermédiaire en réconciliant le diff squelette sur l'année N ou N-1 avec les codes INSEE N et N-1 comme clé de jointure. Vérification que cette procédur s'applique uniquement pour des communes non irisées (type Z).
7. Les cas non expliqués restants sont des créations de communes et d'IRIS et sont documentés comme tel
8. Vérifications de cohérences sur les tables de passages intermédiaires : le code IRIS commence par le code département et l'IRIS n'a pas changé de département de N-1 à N, sauf exception documentée. Voir `DEPARTMENT_CHANGES` dans `settings.toml`.

### A partir de 2013

1. Chargement du [fichier de modifications de l'INSEE consolidées pour cette période](../data/iris/output/modifications_2012_2022.csv).
2. Création d'une table de passage intermédiaire en réconciliant le diff squelette sur l'année N et les éventuelles modifications documentées sur l'année N.

### Jusqu'en 2012

1. Récupération des IRIS réputés modifiés pour l'année N (`MODIF_IRIS != 00`) depuis le diff squelette
2. Réconciliation de ces modifications sur le libellé de l'IRIS entre N-1 et N et création d'une table de passage intermédiaire.
3. Chargement du [fichier de modifications de l'INSEE consolidées pour cette période](../data/iris/output/modifications_1999_2012.csv)].
4. Création d'une table de passage intermédiaire en réconciliant les modifications non expliquées sur l'année N et les éventuelles modifications documentées sur l'année N.

## Table de passage synthétique

On concatène successivement les tables de passage bisannuelles en ordre chronologique inverse : 2022-2021, 2021-2020...

On reporte en amont et en aval la dernière ou la première valeur renseignée pour chaque ligne, sauf dans les cas où une nouvelle commune est créée.

Ex : le code IRIS A est devenu B en 2021, on obtient alors la table suivante (extrait 2019-2022).

| CODE_IRIS_2019 | CODE_IRIS_2020 | CODE_IRIS_2021 | CODE_IRIS_2022 |
| -------------- | -------------- | -------------- | -------------- |
| A              | A              | B              | B              |

## Format de la table passage

Exemple pour le passage de 2015 à 2016.

CODE_IRIS_2015,CODE_IRIS_2016,NATURE_MODIF,DEPCOM_2015,DEPCOM_2016,ANNEE_MODIF,MODIF_IRIS,SOURCE,TYP_IRIS

|          Champ | Description                                            |
|---------------:|--------------------------------------------------------|
| CODE_IRIS_2016 | Code IRIS année N                                      |
|    DEPCOM_2016 | Code INSEE année N                                     |
|    DEPCOM_2015 | Code INSEE année N-1                                   |
| CODE_IRIS_2015 | Code IRIS année N-1                                    |
|         SOURCE | Source utilisée pour la réconciliation                 |
|   NATURE_MODIF | Nature de la modification détectée                     |
|    ANNEE_MODIF | Millésime sur lequel la modification a été détectée    |
|     MODIF_IRIS | Code de modification, venant de l'INSEE ou de Historis |
|       TYP_IRIS | Type de l'IRIS (INSEE)                                 |

### Documentation des variables

[Voir la documentation dédiée](../docs/variables-et-modalites.md).
